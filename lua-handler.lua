-- set LUA_PATH and LUA_CPATH to load also local modules
local prefix_path = './lua';
package.path = ('%s/?.lua;%s/?/init.lua;%s'):format(prefix_path, prefix_path, package.path)
package.cpath = ('%s/?.so;%s/?/init.so;%s'):format(prefix_path, prefix_path, package.cpath)

require("uhttpd-lua-handler")
