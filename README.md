# uHTTPd Lua Utis

Lua handler and request module for uHTTPd webserver.

## Usage

For usage, see [run-uhttpd.sh](./run-uhttpd.sh) and [lua/http-processors/debug.lua](./lua/http-processors/debug.lua).
