local _M = {}

function _M.process(cmd, env)
	uhttpd.send("Status: 200 OK\r\n")
	uhttpd.send("Content-Type: text/plain\r\n\r\n")
	-- see https://github.com/gvzhang/uhttpd/blob/master/uhttpd-lua.c
	uhttpd.send("\r\nenv.REQUEST_METHOD =\r\n	" .. env.REQUEST_METHOD)
	uhttpd.send("\r\nenv.REQUEST_URI =\r\n	" .. env.REQUEST_URI)
	uhttpd.send("\r\nenv.SCRIPT_NAME =\r\n	" .. env.SCRIPT_NAME)
	uhttpd.send("\r\nenv.QUERY_STRING =\r\n	" .. env.QUERY_STRING)
	uhttpd.send("\r\nuhttpd.urldecode(env.QUERY_STRING) =\r\n	" .. uhttpd.urldecode(env.QUERY_STRING))
	uhttpd.send("\r\nenv.PATH_INFO =\r\n	" .. env.PATH_INFO)
	uhttpd.send("\r\nenv.HTTP_VERSION =\r\n	" .. env.HTTP_VERSION)
	uhttpd.send("\r\nenv.SERVER_PROTOCOL =\r\n	" .. env.SERVER_PROTOCOL)
	uhttpd.send("\r\nenv.REMOTE_ADDR =\r\n	" .. env.REMOTE_ADDR)
	uhttpd.send("\r\nenv.REMOTE_PORT =\r\n	" .. env.REMOTE_PORT)
	uhttpd.send("\r\nenv.SERVER_ADDR =\r\n	" .. env.SERVER_ADDR)
	uhttpd.send("\r\nenv.SERVER_PORT =\r\n	" .. env.SERVER_PORT)
	for k,v in pairs(env.headers) do
		uhttpd.send("\r\nenv.headers[" .. k .."] =\r\n	" .. v)
	end
	if (env.CONTENT_TYPE ~= nil) and (env.CONTENT_LENGTH ~= nil) then
		uhttpd.send("\r\nenv.CONTENT_TYPE =\r\n	" .. env.CONTENT_TYPE)
		uhttpd.send("\r\nenv.CONTENT_LENGTH =\r\n	" .. env.CONTENT_LENGTH)
		local content = "";
		repeat
			local rlen, rbuf = uhttpd.recv(4096)
			if (rlen >= 0) then
				content = content .. rbuf
			end
		until (rlen >= 0)
		if (env.CONTENT_TYPE == "application/x-www-form-urlencoded") then
			for form_item in string.gmatch(content, "[^&]+") do
				item_parts = string.gmatch(form_item, "[^=]+")
				uhttpd.send("\r\npost_form[" .. uhttpd.urldecode(item_parts()) .. "] =\r\n	" .. uhttpd.urldecode(item_parts() or ""))
			end
		else
			uhttpd.send("\r\nuhttpd.recv(...) =\r\n	" .. content)
		end
	end
end

return _M
