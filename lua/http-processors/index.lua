local _M = {}

function _M.process(cmd, env)
	uhttpd.send("Status: 404 Not Found\r\n")
	uhttpd.send("Content-Type: text/plain\r\n\r\n")
	uhttpd.send("Nothing here!")
end

return _M
