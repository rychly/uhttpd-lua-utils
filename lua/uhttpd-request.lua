local _M = {}

function _M.uhttpd_recv_all()
	local content = "";
	repeat
		local rlen, rbuf = uhttpd.recv(4096)
		if (rlen >= 0) then
			content = content .. rbuf
		end
	until (rlen >= 0)
	return content
end

function _M.uhttpd_urlencode(plaintext)
	return string.gsub(uhttpd.urlencode(plaintext), " ", "+")
end

function _M.uhttpd_urldecode(urlencoded)
	return string.gsub(uhttpd.urldecode(urlencoded), "%+", " ")
end

function _M.parse_form_data(form_data)
	local parsed = {}
	for form_item in string.gmatch(form_data, "[^&]+") do
		local item_parts = string.gmatch(form_item, "[^=]+")
		local item_key, item_value = item_parts(), item_parts()
		if item_key then
			parsed[_M.uhttpd_urldecode(item_key)] = _M.uhttpd_urldecode(item_value or "");
		end
	end
	return parsed
end

function _M.get_items(env)
	return env.QUERY_STRING and _M.parse_form_data(env.QUERY_STRING) or {}
end

function _M.post_items(env)
	return (env.CONTENT_TYPE == "application/x-www-form-urlencoded") and _M.parse_form_data(_M.uhttpd_recv_all()) or {}
end

function _M.parse_cookie_data(cookie_data)
	local parsed = {}
	for cookie_item in string.gmatch(cookie_data, "[^;]+") do
		local item_parts = string.gmatch(cookie_item, "[^=]+")
		local item_key, item_value = string.gsub(item_parts(), "^%s*(.-)%s*$", "%1"), item_parts()
		if item_key then
			parsed[_M.uhttpd_urldecode(item_key)] = _M.uhttpd_urldecode(item_value or "");
		end
	end
	return parsed
end

function _M.cookies(env)
	return env.headers["cookie"] and _M.parse_cookie_data(env.headers["cookie"]) or {}
end

return _M
