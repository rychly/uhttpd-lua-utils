#!/bin/sh

SERVER_PORT=8080

echo "http://localhost:${SERVER_PORT}/"

exec uhttpd -f -p "${SERVER_PORT}" -h www -l /lua -L ./lua-handler.lua
